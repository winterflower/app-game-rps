import 'package:flutter/material.dart';
import 'dart:math';

class GamePage extends StatefulWidget {
  const GamePage({super.key, required this.title});

  final String title;

  @override
  State<GamePage> createState() => _GamePageState();
}

class _GamePageState extends State<GamePage> {
  int _gamepoint = 10000;
  int _countWin = 0;
  int _countDraw = 0;
  int _countLose = 0;
  String _gameMessage = "";

  int? _myChoice = null;
  int? _comChoice = null;

  final List<Map> _gameItem = [
    { 'name' : '가위', 'imgSrc' : 'assets/가위.jpeg' },
    { 'name' : '바위', 'imgSrc' : 'assets/주먹.jpeg' },
    { 'name' : '보', 'imgSrc' : 'assets/보.jpeg' },
  ];

  void _gameStart(int itemIndex) {
    setState(() {
      if (_gamepoint < 1000) {
        _gameMessage = "포인트가 부족해요! Reset을 누르세요!";
      } else {
        _myChoice = itemIndex;
        _comChoice = Random().nextInt(_gameItem.length);

        if ((_myChoice == 0 && _comChoice == 1) || (_myChoice == 1 && _comChoice == 2) || (_myChoice == 2 && _comChoice == 0)) {
          _countLose += 1;
          _gamepoint -= 1000;
          _gameMessage = "졌어요! 1000점을 잃었어요!";
        } else if ((_myChoice == 0 && _comChoice == 2) || (_myChoice == 1 && _comChoice == 0) || (_myChoice == 2 && _comChoice == 1)) {
          _countWin += 1;
          _gamepoint += 1000;
          _gameMessage = "이겼어요! 1000점을 획득했어요!";
        } else {
          _countDraw += 1;
          _gameMessage = "비겼어요! 다시 도전 해볼까요?";
        }
      }
    });
  }

  void _gameReset() {
    setState(() {
      _gamepoint = 10000;
      _countWin = 0;
      _countDraw = 0;
      _countLose = 0;
      _gameMessage = "";

      _myChoice = null;
      _comChoice = null;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container (
        margin: const EdgeInsets.all(30.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border.all(
          width: 10.0,
          color: Color.fromRGBO(252, 83, 188, 70)
        ),
        color: Colors.white,
        borderRadius: BorderRadius.all(
            Radius.circular(30.0)
        ),
      ),
      child : Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text("Point : $_gamepoint"),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("이김 : $_countWin"),
              Text("비김 : $_countDraw"),
              Text("짐 : $_countLose"),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white
                ),
                  onPressed: () {_gameStart(0);},
                  child: Text("가위",
                  style: TextStyle(shadows: [Shadow(blurRadius: 10.0, color: Colors.black, offset: Offset(0.0, 1.0))]),)
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.yellow,
                      onPrimary: Colors.white
                  ),
                  onPressed: () {_gameStart(1);},
                  child: Text("바위",
                    style: TextStyle(shadows: [Shadow(blurRadius: 10.0, color: Colors.black, offset: Offset(0.0, 1.0))]),)
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      onPrimary: Colors.white
                  ),
                  onPressed: () {_gameStart(2);},
                  child: Text("보",
                    style: TextStyle(shadows: [Shadow(blurRadius: 10.0, color: Colors.black, offset: Offset(0.0, 1.0))]),)
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: [
                  Text("나"),
                  Image.asset(
                    "${_myChoice == null ? 'assets/question-mark.png' : _gameItem[_myChoice!]['imgSrc']}",
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
              Text("VS"),
              Column(
                children: [
                  Text("컴퓨터"),
                  Image.asset(
                    "${_comChoice == null ? 'assets/question-mark.png' : _gameItem[_comChoice!]['imgSrc']}",
                    width: 100,
                    height: 100,
                  ),
                ],
              ),
            ],
          ),
          Text(_gameMessage,
          style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold,
          color: Colors.pinkAccent),
          ),
          TextButton(
              onPressed: () {
                _gameReset();
              },
              style: TextButton.styleFrom(
                primary: Colors.yellowAccent,
                side: BorderSide(
                  color: Colors.pinkAccent, width: 5
                ),
                backgroundColor: Colors.black,
              ),
              child: Text(
                  "Reset",
              style: TextStyle(fontSize: 30.0),
              )
          ),
        ],
      ),
    ),
    );
  }
}
